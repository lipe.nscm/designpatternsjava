package creation.patterns;

public class EagerInitSingleton {

	//Variável estática e privada da mesma classe que será a única instância da classe.
	private static final EagerInitSingleton instancia = new EagerInitSingleton();
	//Construtor privado para restringir a instanciação da classe a partir de outras classes;
	private EagerInitSingleton() {
		
	}
	//Método público que retorna a instância da classe, que servirá como acesso global de todo o 
	//ambiente da aplicação para a classe Singleton.
	public static EagerInitSingleton getInstance() {
		return instancia;
	}
}
