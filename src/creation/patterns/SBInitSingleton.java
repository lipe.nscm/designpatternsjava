package creation.patterns;

import javax.management.RuntimeErrorException;

public class SBInitSingleton {

	//Variável estática contendo a única instância desta classe
	private static SBInitSingleton instancia;
	//Construtor privado
	private SBInitSingleton() {
		
	}
	
	//bloco estático de inicialização com tratamento de exceção
	static {
		try {
			instancia = new SBInitSingleton();
		}catch (Exception e) {
			throw new RuntimeErrorException(null, "Erro ocorrido durante a criação deste Singleton.");
		}
	}
	
	public static SBInitSingleton getInstance() {
		return instancia;
	}
	
}
