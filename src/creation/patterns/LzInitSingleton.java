package creation.patterns;

public class LzInitSingleton {

	private static LzInitSingleton instancia;
	
	private LzInitSingleton() {	}
	
	public static LzInitSingleton getInstance() {
		if(instancia == null) {
			instancia = new LzInitSingleton();
		}
		return instancia;
	}
}
