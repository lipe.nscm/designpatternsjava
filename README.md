1. Singleton
 - O padrão Java Singleton foi originalmente apresentado pelo GoF, Gang of Four, uns caras legais que resolveram organizar e divulgar essa bagaça.
 Pela sua definição, se trata de um DP bastante simples mas que apesar disso, traz consigo algumas peculiaridades que necessitam de atenção durante
sua implementação.
 A implementação de um Singleton sempre traz um ponto divergente entre grupos de programadores, vou tentar nas próximas expor um pouco do que 
 pesquisei e pude extrair dessas formas de implementação do Singleton e algumas práticas legais para seu uso.
 Espero ajudá-los.

1.1 O Padrão Singleton
 a. O padrão restringe a instanciação de uma classe para apenas uma em todo o escopo global.
 b. A nossa classe Singleton deverá prover um ponto de acesso global para obter a instância da classe.
 c. Bastante utilizado para drivers (configuração), log, cache e pool de threads.
 d. Também é utilizado em outros DP´s como Facade, Prototype e Build e etc...
 e. Algumas classes Java Core também fazem uso do DP Singleton como Runtime, e a .awt.Desktop.
 
 1.2 Implementando Singleton
 - Conceitos comuns à implementação: 
 a. Construtor privado para restringir a instanciação da classe a partir de outras classes;
 b. Variável estática e privada da mesma classe que será a única instância da classe.
 c. Método público que retorna a instância da classe, que servirá como acesso global de todo o ambiente da aplicação para a classe Singleton.
 
 1.3 Eager Initialization ( Ou Inicialização Ansiosa hahaha)
 - Nesta forma de criação, a Instância Singleton nasce junto com a classe que está sendo carregada, é um método bastante fácil de criação o problema 
 é que possivelmente ele irá carregar a classe singleton sem haver necessidade de uso da mesma.
- Caso sua Classe Singleton não faça uso de muitos recursos, está uma boa opção de uso. Em casos de uso de uma Classe Singleton para acesso a FileSystem, Base de dados entre outros, nós devemos evitar o uso desta forma pois não oferece tratamento de exceção para possíveis erros, já que dependeremos fortemente de elementos externos.

 1.4 Static Block Initialization ( Ou Inicialização de Bloco Estático )
 - Muito parecido com a inicialização Eager, exceto pelo fato da instância ser criada em um bloco estático com tratamento de exceção.
 - Ambas as formas criam a instância antes mesmo da necessidade direta de seu uso, o que não configura lá uma das melhores práticas.

 1.5 Lazy Initialization ( Ou Inicialização Preguiçosa hehe)
 - Neste caso, é verificado se alguma instância foi criada, e caso não, ela é criada. 
 - Este padrão cria a instância em um método de acesso global.
 - Este tipo de implementação trabalha bem em casos de ambientes single-thread porém, quando se trata de ambientes multithread, isso pode causar alguns problemas se múltiplas 
 threads estiverem dentro da condicional if ao mesmo tempo. Isto iria destruir o padrão Singleton e todas as threads teriam instâncias diferentes da classe Singleton(vê a merda).

 1.6 Thread Safe Singleton
 - Utilizando um método global de criação synchronized, garantimos que apenas uma thread execute este método ao mesmo tempo. 
 - Contudo, ao utilizarmos o modificador synchronized afetamos a perfomance do método em si, ao invés do synchronized podemos utilizar um double-check para verificação antes da criação de nossa instância Singleton. 
